module Charts (
  ChartAssets (..)
, Charts (..)

, drawCharts
) where

import qualified Graphics.Rendering.Chart as C
import qualified Graphics.Rendering.Chart.Axis.Floating as C
import qualified Graphics.Rendering.Chart.Backend.Cairo as CBC
import Diagrams
import Diagrams.Backend.SVG (B, loadImageSVG)
import Data.Colour
import Data.Colour.Names

import Presets
import Data

import Control.Lens
import Data.Default (def)
import Text.Printf (printf)
import Control.Concurrent.Async


data ChartAssets = ChartMonthlyAssets
  { chartPresetMO  :: Preset
  , chartDataMO    :: Data
  }
                 | ChartCompareAssets
  { chartPresetCO  :: Preset
  , chartOldDataCO :: Data
  , chartNewDataCO :: Data
  }

data Charts = ChartsMonthly (Diagram B) (Diagram B)
            | ChartsCompare (Diagram B) (Diagram B)


instance C.BarsPlotValue C.Percent where
    barsReference = 0
    barsAdd       = (+)


sizeSDMonthly = (1102, 1560)
sizeTFMonthly = (1654, 260)
sizeSDCompare = (1654, 1300)
sizeTFCompare = (1654, 260)


drawCharts :: ChartAssets
           -> IO Charts
drawCharts a@(ChartMonthlyAssets {}) = do
  (sd, tf) <- concurrently (drawSDMonthly (chartPresetMO a) (chartDataMO a))
                           (drawTFMonthly (chartDataMO a))
  return $ ChartsMonthly sd tf
drawCharts a@(ChartCompareAssets {}) = do
  (sd, tf) <- concurrently (drawSDCompare (chartPresetCO a) (chartOldDataCO a) (chartNewDataCO a))
                           (drawTFCompare (chartOldDataCO a) (chartNewDataCO a))
  return $ ChartsCompare sd tf


drawSDMonthly :: Preset
              -> Data
              -> IO (Diagram B)
drawSDMonthly preset examData = do
  let r = C.toRenderable layout
      layout = C.layout_plots .~ plots
             $ C.layout_grid_last .~ True
             $ C.layout_x_axis .~ xaxis
             $ C.layout_y_axis .~ yaxis
             $ C.layout_top_axis_visibility .~ def
             $ C.layout_bottom_axis_visibility .~ def
             $ C.layout_right_axis_visibility .~ ( C.axis_show_labels .~ False
                                                 $ C.axis_show_ticks .~ False
                                                 $ def)
             $ def :: C.Layout Int C.Percent

      xaxis  = C.laxis_generate .~ xag
             $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                                $ def)
             $ def

      xag xs = C.makeAxis (map (\x ->  if x == (-1) || x == preset^.exam.totalScore + 1
                                       then ""
                                       else show ((preset^.first2Second)!!x^.second)))
                               (xs, xs, [-1])

      yaxis = C.laxis_generate .~ C.autoScaledAxis
                                   (C.la_labelf .~
                                     map (\(C.Percent d) -> printf "%.1f%%" (d*100))
                                   $ def)
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      l = preset^.exam.lowThreshold
      h = preset^.exam.highThreshold
      ts = preset^.exam.totalScore
      plots = map (C.plotBars . plot) [ ((C.FillStyleSolid (opaque red), Nothing), [0..l-1])
                                      , ((C.FillStyleSolid (opaque yellow), Nothing), [l..h-1])
                                      , ((C.FillStyleSolid (opaque green), Nothing), [h..ts])
                                      , ((C.FillStyleSolid (opaque green), Nothing), [-1, ts+1])
                                      ]
      plot (style, range) = C.plot_bars_values .~
                               map (\x -> (x, [(C.fromValue.(examData^.distribution.func).fromIntegral) x])
                                     ) range 
                          $ C.plot_bars_item_styles .~ [style]
                          $ def
  CBC.renderableToFile (CBC.FileOptions sizeSDMonthly CBC.PNG) "SDMtmp.png" r
  l <- loadImageSVG "SDMtmp.png"
  return l


drawSDCompare :: Preset
              -> Data
              -> Data
              -> IO (Diagram B)
drawSDCompare preset oldData newData = do
  let r = C.toRenderable layout
      layout = C.layout_plots .~ plots
             $ C.layout_grid_last .~ True
             $ C.layout_x_axis .~ xaxis
             $ C.layout_y_axis .~ yaxis
             $ C.layout_top_axis_visibility .~ def
             $ C.layout_bottom_axis_visibility .~ def
             $ C.layout_right_axis_visibility .~ ( C.axis_show_labels .~ False
                                                 $ C.axis_show_ticks .~ False
                                                 $ def)
             $ def :: C.Layout Int C.Percent

      xaxis  = C.laxis_generate .~ xag
             $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                                $ def)
             $ def

      xag xs = C.makeAxis (map (\x ->  if x == (-1) || x == preset^.exam.totalScore + 1
                                       then ""
                                       else show ((preset^.first2Second)!!x^.second)))
                               (xs, xs, [-1])

      yaxis = C.laxis_generate .~ C.autoScaledAxis
                                    (C.la_labelf .~
                                      map (\(C.Percent d) -> printf "%.1f%%" (d*100))
                                    $ def)
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      l = preset^.exam.lowThreshold
      h = preset^.exam.highThreshold
      ts = preset^.exam.totalScore
      plots = map (C.plotBars . plot) [ ([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing)
                                         , (C.FillStyleSolid (opaque red), Nothing)
                                         ], [0..l-1])
                                      , ([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing)
                                         , (C.FillStyleSolid (opaque yellow), Nothing)
                                         ], [l..h-1])
                                      , ([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing)
                                         , (C.FillStyleSolid (opaque green), Nothing)
                                         ], [h..ts])
                                      , ([(C.FillStyleSolid (gray `withOpacity` 0.0), Nothing)
                                         , (C.FillStyleSolid (opaque green), Nothing)
                                         ], [-1, ts+1])
                                      ]
      plot (styles, range) = C.plot_bars_values .~
                               map (\x -> (x, [ (C.fromValue.(oldData^.distribution.func).fromIntegral) x
                                              , (C.fromValue.(newData^.distribution.func).fromIntegral) x
                                              ]
                                          )
                                   ) range
                          $ C.plot_bars_item_styles .~ styles
                          $ def

  CBC.renderableToFile (CBC.FileOptions sizeSDCompare CBC.PNG) "SDCtmp.png" r
  l <- loadImageSVG "SDCtmp.png"
  return l



drawTFMonthly :: Data
              -> IO (Diagram B)
drawTFMonthly examData = do
  let r = C.toRenderable layout
      layout = C.layout_plots .~ plots
             $ C.layout_grid_last .~ True
             $ C.layout_x_axis .~ xaxis
             $ C.layout_y_axis .~ yaxis
             $ C.layout_right_axis_visibility .~ ( C.axis_show_labels .~ False
                                                 $ C.axis_show_ticks .~ False
                                                 $ def)
             $ C.layout_top_axis_visibility .~ ( C.axis_show_labels .~ False
                                               $ C.axis_show_ticks .~ False
                                               $ def)
             $ def :: C.Layout Int C.Percent

      tf = examData^.frequencies
      lows = filter (\freq -> freq^.rank == Low) tf
      meds = filter (\freq -> freq^.rank == Medium) tf
      highs = filter (\freq -> freq^.rank == High) tf
      plots = map (C.plotBars . plot)
                (filter (\ (_, vs) -> not (null vs))
                  [((C.FillStyleSolid (opaque red), Nothing), lows),
                   ((C.FillStyleSolid (opaque yellow), Nothing), meds),
                   ((C.FillStyleSolid (opaque green), Nothing), highs)
                  ]
                )

      xaxis = C.laxis_generate .~ xag
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      xag xs = C.makeAxis (map (\x -> if x == 0 || x == length tf + 1
                                      then ""
                                      else show x
                               )
                          ) (xs, xs, [0])

      yaxis = C.laxis_generate .~ C.scaledAxis
                                    (C.la_labelf .~
                                      map (\(C.Percent d) -> printf "%.0f%%" (d*100))
                                    $ def)
                                    (0.0, 1.0)
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      plot (style, dt) = C.plot_bars_values .~ (0,[C.Percent 0])
                                             : map (\f -> (f^.task, [C.fromValue (f^.freq)])) dt
                                            ++ [(last dt^.task+1, [C.Percent 0])]
                       $ C.plot_bars_spacing .~ C.BarsFixWidth (fromIntegral (fst sizeTFMonthly)
                                                             / (fromIntegral (length tf+2)*1.5))
                       $ C.plot_bars_item_styles .~ [style]
                       $ def

  CBC.renderableToFile (CBC.FileOptions sizeTFMonthly CBC.PNG) "TFMtmp.png" r
  l <- loadImageSVG "TFMtmp.png"
  return l


drawTFCompare :: Data
              -> Data
              -> IO (Diagram B)
drawTFCompare oldData newData = do
  let r = C.toRenderable layout
      layout = C.layout_plots .~ plots
             $ C.layout_grid_last .~ True
             $ C.layout_x_axis .~ xaxis
             $ C.layout_y_axis .~ yaxis
             $ C.layout_right_axis_visibility .~ ( C.axis_show_labels .~ False
                                                 $ C.axis_show_ticks .~ False
                                                 $ def)
             $ C.layout_top_axis_visibility .~ ( C.axis_show_labels .~ False
                                               $ C.axis_show_ticks .~ False
                                               $ def)
             $ def :: C.Layout Int C.Percent

      otf = oldData^.frequencies
      ntf = newData^.frequencies
      otfntf = zip otf ntf
   
      lows = filter (\(_, freq) -> freq^.rank == Low) otfntf
      meds = filter (\(_, freq) -> freq^.rank == Medium) otfntf
      highs = filter (\(_, freq) -> freq^.rank == High) otfntf
      plots = map (C.plotBars . plot)
                (filter (\ (_, vs) -> not (null vs))
                  [([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing),
                     (C.FillStyleSolid (opaque red), Nothing)
                    ], lows),
                   ([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing),
                     (C.FillStyleSolid (opaque yellow), Nothing)
                    ], meds),
                   ([(C.FillStyleSolid (gray `withOpacity` 0.2), Nothing),
                     (C.FillStyleSolid (opaque green), Nothing)
                    ], highs)
                  ]
                )

      xaxis = C.laxis_generate .~ xag
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      xag xs = C.makeAxis (map (\x -> if x == 0 || x == length otf + 1
                                      then ""
                                      else show x
                               )
                          ) (xs, xs, [0])

      yaxis = C.laxis_generate .~ C.scaledAxis
                                    (C.la_labelf .~
                                      map (\(C.Percent d) -> printf "%.0f%%" (d*100))
                                    $ def)
                                    (0.0, 1.0)
            $ C.laxis_style .~ (C.axis_label_style .~ (C.font_size .~ 25 $ def)
                               $ def)
            $ def

      plot (styles, dt) = C.plot_bars_values .~
                            (0,[C.Percent 0])
                          : map (\(oldf, newf) -> (oldf^.task, [C.fromValue (oldf^.freq), C.fromValue (newf^.freq)])) dt
                          ++ [(length otf+1, [C.Percent 0])]
                       $ C.plot_bars_spacing .~ C.BarsFixWidth (fromIntegral (fst sizeTFCompare)
                                                             / (fromIntegral (length otf+2)*3))
                       $ C.plot_bars_item_styles .~ styles
                       $ def

  CBC.renderableToFile (CBC.FileOptions sizeTFCompare CBC.PNG) "TFCtmp.png" r
  l <- loadImageSVG "TFCtmp.png"
  return l
