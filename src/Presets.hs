{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}


module Presets (
  Preset (..)
, Exam
, First2Second
, MaxTaskScore

, getPreset

, exam
, first2Second
, maxTaskScores
, name
, totalScore
, lowThreshold
, highThreshold
, first
, second
, taskNumber
, maxScore
, lowFreq
, highFreq
) where


import qualified Data.ByteString.Lazy as BL
import qualified Data.Vector as V
import qualified Data.Csv as CSV
import qualified System.IO.Error as IOE
import System.EasyFile ((</>))
import Control.Lens
import GHC.Generics


data Exam = Exam
  { _name          :: String
  , _totalScore    :: Int
  , _lowThreshold  :: Int
  , _highThreshold :: Int
  } deriving (Generic)

data First2Second = F2S
  { _first  :: Int
  , _second :: Int
  } deriving (Generic)

data MaxTaskScore = MTS
  { _taskNumber :: Int
  , _maxScore   :: Int
  , _lowFreq    :: Double
  , _highFreq   :: Double
  } deriving (Generic)

data Preset = Preset
  { _exam          :: Exam
  , _first2Second  :: [First2Second]
  , _maxTaskScores :: [MaxTaskScore]
  } deriving (Generic)


instance CSV.FromRecord Exam
instance CSV.FromRecord First2Second
instance CSV.FromRecord MaxTaskScore


makeLenses ''Exam
makeLenses ''First2Second
makeLenses ''MaxTaskScore
makeLenses ''Preset


getPreset :: FilePath
          -> IO (Either String Preset)
getPreset presetDir = (flip IOE.catchIOError) (return . Left . show) $ do
  [rawExam, rawF2S, rawMTS] <- mapM (BL.readFile . (presetDir </>)) ["exam.csv", "f2s.csv", "mts.csv"]
  let csvExam = CSV.decode CSV.NoHeader rawExam
      csvF2S  = CSV.decode CSV.NoHeader rawF2S
      csvMTS  = CSV.decode CSV.NoHeader rawMTS
  return $ do
    exam <- csvExam
    f2s <- csvF2S
    mts <- csvMTS
    Right $ Preset (exam V.! 0) (V.toList f2s) (V.toList mts)
