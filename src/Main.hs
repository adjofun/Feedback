module Main where

import Diagrams hiding (value)
import Diagrams.Backend.SVG

import Presets
import Data
import Charts
import Report

import Data.List
import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Data.Time (getCurrentTime, utctDay)
import Options.Applicative
import Data.Semigroup ((<>))
import System.EasyFile
import System.Process
import Control.Exception

import qualified Data.Conduit as C
import qualified Data.Conduit.List as C
import qualified Data.Conduit.Async as CA


data MonthlyOptions = MO {
  presetdirMO :: FilePath
, logodirMO :: FilePath
, datadirMO :: FilePath
, outdirMO :: FilePath
} deriving (Show)

data CompareOptions = CO {
  presetdirCO :: FilePath
, logodirCO :: FilePath
, olddatadirCO :: FilePath
, newdatadirCO :: FilePath
, outdirCO :: FilePath
} deriving (Show)

data Task = MonthlyJob MonthlyOptions
          | CompareJob CompareOptions

data Opts = Opts Task


mo = MO <$> strOption
            (short 'p' <> long "presets" <> metavar "DIR"
            <> value "presets" <> showDefault <> help "Directory with exam presets"
            )
        <*> strOption
            (short 'l' <> long "logos" <> metavar "DIR"
            <> value "logos" <> showDefault <> help "Directory with school logotips"
            )
        <*> strOption
            (short 'd' <> long "data" <> metavar "DIR"
            <> value "data" <> showDefault <> help "Directory with exam data"
            )
        <*> strOption
            (short 'o' <> long "output" <> metavar "DIR"
            <> value "output" <> showDefault <> help "Directory to output results"
            )

co = CO <$> strOption
            (short 'p' <> long "presets" <> metavar "DIR"
            <> value "presets" <> showDefault <> help "Directory with exam presets"
            )
        <*> strOption
            (short 'l' <> long "logos" <> metavar "DIR"
            <> value "logos" <> showDefault <> help "Directory with school logotips"
            )
        <*> strOption
            (short 'O' <> long "olddata" <> metavar "DIR"
            <> help "Directory with old exam data"
            )
        <*> strOption
            (short 'N' <> long "newdata" <> metavar "DIR"
            <> help "Directory with new exam data"
            )
        <*> strOption
            (short 'o' <> long "output" <> metavar "DIR"
            <> value "output" <> showDefault <> help "Directory to output results"
            )

reportJob = MonthlyJob
             <$> hsubparser (command "monthly"
                   (info mo (progDesc "Generate monthly reports")))
         <|> CompareJob
             <$> hsubparser (command "compare"
                   (info co (progDesc "Generate comparing reports")))

options = Opts <$> reportJob


main = do
  Opts job <- execParser $ info (options <**> helper) fullDesc
  case job of
    MonthlyJob mopts -> do
      fs <- getDirectoryContents (datadirMO mopts)
      createDirectoryIfMissing False (outdirMO mopts)
      CA.runCConduit $
        CA.buffer' 10
          (    C.sourceList fs
          C..| C.filter (\fp -> Prelude.head fp /= '.')
          C..| splitC
          C..| logoC (logodirMO mopts)
          C..| presetC (presetdirMO mopts)
          C..| dataMC (datadirMO mopts)
          C..| reportMC
          )
          (
          pdfSink (outdirMO mopts)
          )
    CompareJob copts -> do
      ofs <- getDirectoryContents (olddatadirCO copts)
      nfs <- getDirectoryContents (newdatadirCO copts)
      createDirectoryIfMissing False (outdirCO copts)
      CA.runCConduit $
        CA.buffer' 10
          (    C.sourceList (ofs `intersect` nfs)
          C..| C.filter (\fp -> Prelude.head fp /= '.')
          C..| splitC
          C..| logoC (logodirCO copts)
          C..| presetC (presetdirCO copts)
          C..| dataCC (olddatadirCO copts) (newdatadirCO copts)
          C..| reportCC (olddatadirCO copts) (newdatadirCO copts)
          )
          (
          pdfSink (outdirCO copts)
          )


splitC = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just fp -> do
      let l = splitBy '-' (dropExtensions fp)
      if Prelude.length l /= 3
      then do liftIO $ putStrLn ("Bad filename: " ++ fp)
      else do
        let logo   = l!!0
            name   = l!!1
            preset = l!!2
        C.yield (logo, preset, fp, name)
      splitC

logoC logoDir = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, fp, name) -> do
      eitherLogo <- liftIO $ loadImageEmb (logoDir </> logo)
      case eitherLogo of
        Left err -> liftIO (putStrLn err)
        Right lgo -> C.yield (image lgo :: Diagram B, preset, fp, name)
      logoC logoDir

presetC presetDir = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, fp, name) -> do
      eitherPreset <- liftIO $ getPreset (presetDir </> preset)
      case eitherPreset of
        Left err -> liftIO (putStrLn err)
        Right pre -> C.yield (logo, pre, fp, name)
      presetC presetDir

dataMC dataDir = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, fp, name) -> do
      eitherData <- liftIO $ getData (dataDir </> fp) preset
      case eitherData of
        Left err -> liftIO (putStrLn err)
        Right dat -> C.yield (logo, preset, dat, fp, name)
      dataMC dataDir

dataCC oldDataDir newDataDir = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, fp, name) -> do
      eitherData <- liftIO $ getData (oldDataDir </> fp) preset
      case eitherData of
        Left err -> liftIO (putStrLn err)
        Right old -> do
          eitherData <- liftIO $ getData (newDataDir </> fp) preset
          case eitherData of
            Left err -> liftIO (putStrLn err)
            Right new -> C.yield (logo, preset, old, new, fp, name)
      dataCC oldDataDir newDataDir

reportMC = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, dat, fp, name) -> do
      time <- liftIO getCurrentTime
      let assets = ReportAssets (Monthly dat) preset logo
      report <- liftIO $ makeReport (utctDay time) name assets
      C.yield (report, fp)
      reportMC

reportCC oldStr newStr = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (logo, preset, oldData, newData, fp, name) -> do
      time <- liftIO getCurrentTime
      let assets = ReportAssets (Compare (oldStr, oldData) (newStr, newData)) preset logo
      report <- liftIO $ makeReport (utctDay time) name assets
      C.yield (report, fp)
      reportCC oldStr newStr

pdfSink outDir = do
  upstream <- C.await
  case upstream of
    Nothing -> return ()
    Just (report, fp) -> do
      let [school, name, exam] = splitBy '-' (dropExtension fp)
      liftIO $ createDirectoryIfMissing False (outDir </> school)
      liftIO $ createDirectoryIfMissing False (outDir </> school </> exam)
      let nfp = outDir </> school </> exam </> name <.> "svg"
      liftIO $ renderSVG nfp (dims (r2 (1654, 2339))) report
      pdf <- liftIO $ (try (callProcess "inkscape" [nfp, "-A", (dropExtension nfp <.> "pdf")]) :: IO (Either SomeException ()))
      case pdf of
        Left err -> liftIO $ putStrLn (show err)
        Right () -> liftIO $ removeFile nfp
      pdfSink outDir

splitBy delimiter = foldr f [[]] 
  where f c l@(x:xs) | c == delimiter = []:l
                     | otherwise = (c:x):xs
