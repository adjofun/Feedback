module Report (
  ReportAssets (..)
, ReportType (..)
, makeReport
) where

import Diagrams
import Diagrams.TwoD
import Diagrams.TwoD.Text
import Data.Colour
import Data.Colour.Names
import Diagrams.Backend.SVG (B)

import Presets
import Data
import Charts

import Data.Monoid ((<>))
import Text.Printf (printf)
import Data.Time (Day)
import Control.Lens hiding ((#))

data ReportType = Monthly Data
                | Compare (String, Data) (String, Data)

data ReportAssets = ReportAssets {
                    rType :: ReportType
                  , rPreset :: Preset
                  , rLogo :: Diagram B
                  }

text' :: String 
      -> Double
      -> Diagram B
text' string size = alignedText 0.5 0.5 string # fontSize (local size)

box :: Double
    -> Double
    -> String
    -> Double
    -> Maybe (AlphaColour Double)
    -> Diagram B
box x y string size color = (text' string size 
                            <> rect x y # lwL 0.5 # case color of
                                  Just c -> fcA c
                                  _ -> id
                            ) # alignBL :: Diagram B

makeReport :: Day
           -> String
           -> ReportAssets
           -> IO (Diagram B)
makeReport date studentName assets = do
  let ReportAssets rType preset logo = assets
      printRanges f hr mr fr =
        let rh = sum $ map (f.fromIntegral) hr
            rm = sum $ map (f.fromIntegral) mr
            rf = sum $ map (f.fromIntegral) fr
            hi = printf "%.2f%%" ((100*rh) :: Double)
            me = printf "%.2f%%" ((100*rm) :: Double)
            fa = printf "%.2f%%" ((100*rf) :: Double)
        in (hi, me, fa)
      ex = preset^.exam
      f2s = preset^.first2Second
      mts = preset^.maxTaskScores
      ts = ex^.totalScore
      h = ex^.highThreshold
      l = ex^.lowThreshold
      sh = "Балл ≥ " ++ show ((f2s!!h)^.second)
      sm =  show ((f2s!!l)^.second)
         ++ " ≥ Балл < "
         ++ show ((f2s!!h)^.second)
      sf = "Балл < " ++ show ((f2s!!l)^.second)
      header =  rect 1654 520 # alignBL # lwL 0.5
             <> logo # alignT # translate (r2 (827, 520))
             <> box 550  120 "Имя и фамилия" 60 Nothing # translate (r2 (0,   120))
             <> box 1104 120 studentName     50 Nothing # translate (r2 (550, 120))
             <> box 550  120 "Экзамен"       60 Nothing # translate (r2 (0,   0))
             <> box 1104 120 (ex^.name)      50 Nothing # translate (r2 (550, 0))
  case rType of
       Monthly examData -> do
         let distr = examData^.distribution
             lowerBound  = (f2s !! round (distr^.mean - distr^.stdDev))^.second
             higherBound = (f2s !! round (distr^.mean + distr^.stdDev))^.second
             dp   = printf "от %d до %d" lowerBound higherBound
             (high, med, fail) = printRanges (distr^.func) [h..ts] [l..h-1] [0..l-1]
             datestr = "Дата проверки: " ++ show date

             bg = rect 1654 2339 # alignBL

             aside =  rect 550 1560 # alignBL # lwL 0.5
                   <> box 550 100 "Тестовый балл" 40 (Just (blue   `withOpacity` 0.2)) # translate (r2 (0, 1460))
                   <> box 550 160 dp              50 (Just (blue   `withOpacity` 0.1)) # translate (r2 (0, 1300))
                   <> box 550 100 sh              40 (Just (green  `withOpacity` 0.2)) # translate (r2 (0, 1200))
                   <> box 550 160 high            50 (Just (green  `withOpacity` 0.1)) # translate (r2 (0, 1040))
                   <> box 550 100 sm              40 (Just (yellow `withOpacity` 0.2)) # translate (r2 (0, 940))
                   <> box 550 160 med             50 (Just (yellow `withOpacity` 0.1)) # translate (r2 (0, 780))
                   <> box 550 100 sf              40 (Just (red    `withOpacity` 0.2)) # translate (r2 (0, 680))
                   <> box 550 160 fail            50 (Just (red    `withOpacity` 0.1)) # translate (r2 (0, 520))
                   <> box 550 100 "Заметки"       40 Nothing                           # translate (r2 (0, 420))
                   <> box 550 420 ""              0  Nothing                           # translate (r2 (0, 0))
                   <> box 550 50  datestr         25 Nothing                           # translate (r2 (0, 0))

         ChartsMonthly sdi tfi <- drawCharts (ChartMonthlyAssets preset examData)

         let dia =  bg
                 <> header # alignBL # translate (r2 (0,    1819))
                 <> aside  # alignBL # translate (r2 (1104, 259))
                 <> sdi    # alignBL # translate (r2 (0,    260))
                 <> tfi    # alignBL # translate (r2 (0,    0))
         return dia

       Compare (oldDate, oldData) (newDate, newData) -> do
         let olddistr = oldData^.distribution
             newdistr = newData^.distribution
             oldlowerBound  = (f2s !! round (olddistr^.mean - olddistr^.stdDev))^.second
             oldhigherBound = (f2s !! round (olddistr^.mean + olddistr^.stdDev))^.second
             newlowerBound  = (f2s !! round (newdistr^.mean - newdistr^.stdDev))^.second
             newhigherBound = (f2s !! round (newdistr^.mean + newdistr^.stdDev))^.second
             olddp   = printf "от %d до %d" oldlowerBound oldhigherBound
             newdp   = printf "от %d до %d" newlowerBound newhigherBound
             (oldhigh, oldmed, oldfail) = printRanges (olddistr^.func) [h..ts] [l..h-1] [0..l-1]
             (newhigh, newmed, newfail) = printRanges (newdistr^.func) [h..ts] [l..h-1] [0..l-1]

             bg = rect 1654 2339 # alignBL
             table = rect 1654 260 # alignBL # lwL 0.5
                   <> box 182 30  "Дата проверки" 20 Nothing                           # translate (r2 (0, 230))
                   <> box 182 30  (show date)     20 Nothing                           # translate (r2 (0, 200))
                   <> box 182 100 oldDate         30 (Just (blue   `withOpacity` 0.2)) # translate (r2 (0,    100))
                   <> box 182 100 newDate         30 (Just (blue   `withOpacity` 0.2)) # translate (r2 (0,    0))
                   <> box 368 60  "Тестовый балл" 30 (Just (blue   `withOpacity` 0.2)) # translate (r2 (182,  200))
                   <> box 368 100 olddp           40 (Just (blue   `withOpacity` 0.1)) # translate (r2 (182,  100))
                   <> box 368 100 newdp           40 (Just (blue   `withOpacity` 0.1)) # translate (r2 (182,  0))
                   <> box 368 60  sh              30 (Just (green  `withOpacity` 0.2)) # translate (r2 (550,  200))
                   <> box 368 100 oldhigh         40 (Just (green  `withOpacity` 0.1)) # translate (r2 (550,  100))
                   <> box 368 100 newhigh         40 (Just (green  `withOpacity` 0.1)) # translate (r2 (550,  0))
                   <> box 368 60  sm              30 (Just (yellow `withOpacity` 0.2)) # translate (r2 (918,  200))
                   <> box 368 100 oldmed          40 (Just (yellow `withOpacity` 0.1)) # translate (r2 (918,  100))
                   <> box 368 100 newmed          40 (Just (yellow `withOpacity` 0.1)) # translate (r2 (918,  0))
                   <> box 368 60  sf              30 (Just (red    `withOpacity` 0.2)) # translate (r2 (1286, 200))
                   <> box 368 100 oldfail         40 (Just (red    `withOpacity` 0.1)) # translate (r2 (1286, 100))
                   <> box 368 100 newfail         40 (Just (red    `withOpacity` 0.1)) # translate (r2 (1286, 0))
         ChartsCompare sdi tfi <- drawCharts (ChartCompareAssets preset oldData newData)
         let dia =  bg
                 <> header # alignBL # translate (r2 (0,    1819))
                 <> table  # alignBL # translate (r2 (0,    1559))
                 <> sdi    # alignBL # translate (r2 (0,    260))
                 <> tfi    # alignBL # translate (r2 (0,    0))
         return dia
